import Vue from 'vue'
import Router from 'vue-router'
import nuevoTicket from '@/components/nuevoTicket'
import BootstrapVue from 'bootstrap-vue'
import ticket from '@/components/ticketSolo'
import VueResource from "vue-resource";
import listadoTickets from '@/components/listadoTickets'
import histo from '@/components/historial'
import envios from '@/components/envios'
import profile from '@/components/profile'
import inicio from '@/components/inicio'
import firebase from 'firebase';
import auth from './auth-guard'

Vue.use(Router)
Vue.use(BootstrapVue);
Vue.use(VueResource);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Inicio',
      component: inicio,
    },
    {
      path: '/profile',
      name: 'Perfil',
      component: profile,
      beforeEnter: auth
    },
    {
      path: '/envios',
      name: 'Envios',
      component: envios,
      beforeEnter: auth
    },
    {
      path: '/historial',
      name: 'Historial',
      component: histo
    },
    {
      path: '/listadoTickets',
      name: 'Listado',
      component: listadoTickets
    },
    {
      path: '/nuevoTicket',
      name: 'Home',
      component: nuevoTicket
    },
    {
      path: '/listadoTickets/:id',
      name: 'Ticket',
      props: true,
      component: ticket
    },
  ],

  mode: 'history'
})
