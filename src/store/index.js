import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    loading: false,
    loadingRegistro: false,
    error: null,
    errorRegistro: null,
    titulo: 'PuertoAPP'
  },
  mutations: {
    setUser (state, payload){
      state.user = payload
    },
    setLoading (state, payload){
      state.loading = payload
    },
    setLoadingRegistro (state, payload){
      state.loadingRegistro = payload
    },
    setError (state, payload){
      state.error = payload
    },
    setErrorRegistro (state, payload){
      state.errorRegistro = payload
    },
    clearError (state){
      state.error = null
    },
    clearErrorRegistro (state){
      state.errorRegistro = null
    },
    setTitulo (state, payload){
      state.titulo = payload
    }
  },
  actions: {
    signUserUp ({commit}, payload){
      commit('setLoadingRegistro', true)
      commit('clearErrorRegistro')
      firebase.auth().createUserWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoadingRegistro', false)
            const newUser = {
              id: user.uid,
            }
            commit('setUser', newUser)
          }
        )
        .catch(
          error => {
            console.log(error)
            commit('setLoadingRegistro', false)
            commit ('setErrorRegistro', error)
          }
        )
    },
    signUserIn ({commit}, payload){
      commit('setLoading', true)
      commit('clearError')
      firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
        .then(
          user => {
            commit('setLoading', false)
            const newUser = {
            id: user.uid,
          }
            commit('setUser', newUser)}
        )
        .catch(
          error => {
            commit('setLoading', false)
            commit ('setError', error)
            console.log(error)
          }
        )
    },
    autoSignIn ({commit}, payload){
      commit('setUser', {id: payload.uid})
    },
    logOut({commit}){
      firebase.auth().signOut()
      commit('setUser', null)
    },
    clearError ({commit}) {
      commit('clearError')
    },
    clearErrorRegistro ({commit}) {
      commit('clearErrorRegistro')
    },
    changeTitle({commit}, payload){
      commit('setTitulo', payload)
    }
  },
  getters: {
    user (state){
      return state.user
    },
    loading (state){
      return state.loading
    },
    loadingRegistro (state){
      return state.loadingRegistro
    },
    error (state){
      return state.error
    },
    errorRegistro (state){
      return state.errorRegistro
    },
    titulo (state){
      return state.titulo
    }
  },

})
