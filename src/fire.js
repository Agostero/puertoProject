var firebase = require('firebase')

var config = {
  apiKey: "AIzaSyB5P1jVC_gBgbeW43J2E2o-hznZPEF6a-0",
  authDomain: "puerto-1ac59.firebaseapp.com",
  databaseURL: "https://puerto-1ac59.firebaseio.com",
  projectId: "puerto-1ac59",
  storageBucket: "puerto-1ac59.appspot.com",
  messagingSenderId: "249897714301"
};
var firebaseApp = firebase.initializeApp(config);
var db = firebaseApp.database();

export default db;
