// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VueFire from 'vuefire'
import Vuetify from 'vuetify'
import { store } from './store'
import alert from './components/alert.vue'
import * as firebase from 'firebase'


Vue.config.productionTip = false
Vue.use(VueResource);
Vue.use(VueFire);
Vue.use(Vuetify);
Vue.component('app-alert', alert)


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created(){
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
      }
    })
  }
});
